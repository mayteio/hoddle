import React from "react";
import { makeStyles } from "@material-ui/styles";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(theme => {
  return {
    paper: {
      padding: `${theme.spacing(1.5)}px ${theme.spacing(3)}px`,
      marginBottom: `${theme.spacing(1.5)}px`,
      display: "inline-block",
      borderBottomLeftRadius: "0 !important",
      maxWidth: ({ width }) => width
    }
  };
});

function MessageBubble({ children, width = 500, ...props }) {
  const classes = useStyles({ width });
  return (
    <Paper className={classes.paper} {...props}>
      {children}
    </Paper>
  );
}

export default MessageBubble;
