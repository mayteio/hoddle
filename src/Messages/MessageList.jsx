import React from "react";
import { animated, useTransition } from "react-spring";

function MessageList({ messages }) {
  const transitions = useTransition(messages, item => item.key, states);

  return (
    <>
      {transitions.map(({ item, props, key }) => (
        <animated.div key={key} style={props}>
          {item}
        </animated.div>
      ))}
    </>
  );
}

export default MessageList;

const states = {
  from: {
    transform: "translate3d(-100px, 20px, -20px)",
    opacity: 0
  },
  enter: {
    transform: "translate3d(0%, 0, 0)",
    opacity: 1
  },
  leave: {
    transform: "translate3d(-100%, 0, 0)",
    opacity: 0
  },
  config: { friction: 20, mass: 1, tension: 500 },
  trail: 50
};
