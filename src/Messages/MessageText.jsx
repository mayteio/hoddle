import React from "react";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";

function MessageText({ children, ...props }) {
  return (
    <Typography variant="body2" {...props}>
      {children}
    </Typography>
  );
}

MessageText.propTypes = {
  children: PropTypes.string.isRequired
};

export default MessageText;
