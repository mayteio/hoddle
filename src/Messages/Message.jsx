import React from "react";
import PropTypes from "prop-types";
import MessageBubble from "./MessageBubble";
import MessageText from "./MessageText";

function Message({ text, children, width, ...props }) {
  return (
    <>
      {children && children}
      {text && (
        <MessageBubble width={width} {...props}>
          <MessageText>{text}</MessageText>
        </MessageBubble>
      )}
    </>
  );
}

Message.propTypes = {
  text: PropTypes.string,
  width: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
};

export default Message;
