import React from "react";
import { storiesOf } from "@storybook/react";

import Emoji from "a11y-react-emoji";
import Message from "./Message";
import MessageText from "./MessageText";
import MessageBubble from "./MessageBubble";
import MessageList from "./MessageList";

const text = (
  <>
    Hello <Emoji symbol="👋" />
  </>
);
const MessageTextComponent = () => <MessageText>{text}</MessageText>;
const MessageHelperComponent = () => <Message text={text} />;

storiesOf("Messages", module)
  .add("Message Text", () => <MessageTextComponent />)
  .add("Message Bubble", () => (
    <MessageBubble>
      <MessageTextComponent />
    </MessageBubble>
  ))
  .add("Message Helper", () => <MessageHelperComponent />)
  .add("Message List", () => (
    <MessageList
      messages={[
        <MessageHelperComponent key={1} />,
        <MessageHelperComponent key={2} />
      ]}
    />
  ));
