import React from "react";
import { storiesOf } from "@storybook/react";
import { Provider } from "react-redux";
import store from "../common/store";
import InteractiveMap from "./InteractiveMap";
import {MapProvider} from "./MapProvider";
import ParticipateMap from "./ParticipateMap";
import MapboxMap from "./MapboxMap";
import PopupMap from "./PopupMap";

const withReduxProvider = story => <Provider store={store}>{story()}</Provider>;
const withMapProvider = story => <MapProvider>{story()}</MapProvider>;

storiesOf("Maps", module)
  .addDecorator(withReduxProvider)
  .addDecorator(withMapProvider)
  .add("Default Map", () => <InteractiveMap />)
  .add("Mapbox Layers Map", () => <MapboxMap />)
  .add("Map with popup", () => <PopupMap />)
  .add("Participate Map", () => <ParticipateMap />);
