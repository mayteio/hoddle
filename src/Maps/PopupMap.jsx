import React from "react";
import Typography from "@material-ui/core/Typography";
import Popup from "./Popup";
import InteractiveMap from "./InteractiveMap";

function PopupMap() {
  const layout = {
    longitude: 144.96,
    latitude: -37.81
  };
  return (
    <InteractiveMap>
      <Popup {...layout}>
        <Typography variant="h4">JSX here</Typography>
        <Typography variant="body2">
          TODO: add action buttons & markers, style more like com
        </Typography>
      </Popup>
    </InteractiveMap>
  );
}

export default PopupMap;
