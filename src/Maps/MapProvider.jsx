import React, { useState, useRef } from "react";

export const MapContext = React.createContext({});
const { Provider, Consumer } = MapContext;
export const MapConsumer = Consumer;

export const DEFAULT_VIEW_STATE = {
  latitude: -37.8136,
  longitude: 144.96332,
  zoom: 13,
  pitch: 0,
  bearing: 0,
  touchRotate: true,
  dragPan: false
};

export function MapProvider({ children }) {
  /**
   *
   * Global view state for the map.
   *
   *
   */
  const [viewState, setViewState] = useState({
    ...DEFAULT_VIEW_STATE
  });

  /**
   * Fly to views smoothly
   */
  const flyTo = vs =>
    setViewState(viewState => ({
      ...viewState,
      ...vs
    }));

  /**
   * Hold global ref to mapbox instance
   */
  const mapRef = useRef();
  const deckRef = useRef();

  return (
    <Provider
      value={{
        viewState,
        setViewState,
        flyTo,
        mapRef,
        deckRef
      }}
    >
      {children}
    </Provider>
  );
}
