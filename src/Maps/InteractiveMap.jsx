import React, { useEffect, useContext, useState, useRef } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import { StaticMap } from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import DeckGL from "@deck.gl/react";
import { MapContext } from "./MapProvider";
import { loadMapStyle } from "../reducers/mapboxStyle";

function InteractiveMap({
  mapboxStyle = "mapbox/dark-v9",
  mapboxApiAccessToken = "no-token",
  onStyleLoaded = () => {},
  children,
  ...props
}) {
  /**
   * When the map is ready, get the current style from
   * the API ready to be modified in the store
   */
  const dispatch = useDispatch();
  function onLoad() {
    dispatch(loadMapStyle({ styleId: mapboxStyle }));
  }

  /**
   * Wait for style to load before adding any sources or layers
   */
  const style = useSelector(state => state.map.style);
  useEffect(() => {
    if (style.get("apiLoaded") && style.size !== 0) {
      onStyleLoaded();
    }
  }, [style.get("apiLoaded"), onStyleLoaded]);

  /**
   * Get style and tokens to display the map
   */
  const mapStyle = useSelector(state => state.map.style);
  const token =
    process.env.REACT_MAPBOX_ACCESS_TOKEN ||
    process.env.STORYBOOK_MAPBOX_ACCESS_TOKEN ||
    mapboxApiAccessToken;

  const { viewState, setViewState, mapRef, deckRef } = useContext(MapContext);
  const [gl, setGl] = useState(undefined);

  return (
    <DeckGL
      controller={{ touchRotate: true }}
      viewState={viewState}
      onViewStateChange={({ viewState }) => setViewState(viewState)}
      // onWebGLInitialized={gl => setGl(gl)}
      ref={ref => (deckRef.current = ref && ref.deck)}
      {...props}
    >
      {/* {gl && ( */}
      <StaticMap
        onLoad={onLoad}
        mapStyle={
          mapStyle && mapStyle.get("apiLoaded")
            ? mapStyle
            : `mapbox://styles/${mapboxStyle}`
        }
        mapboxApiAccessToken={token}
        ref={ref => (mapRef.current = ref && ref.getMap())}
        // gl={gl}
      >
        {children}
      </StaticMap>
      {/* )} */}
    </DeckGL>
  );
}

InteractiveMap.propTypes = {
  mapboxStyle: PropTypes.string,
  mapboxApiAccessToken: PropTypes.string,
  onStyleLoaded: PropTypes.func,
  children: PropTypes.node
};

export default InteractiveMap;
