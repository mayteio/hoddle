import React from "react";
import PropTypes from "prop-types";
import { Popup as MapboxPopup } from "react-map-gl";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
  root: {
    minWidth: theme.spacing(10),
    zIndex: 8,
    "& .mapboxgl-popup": {},
    "& .mapboxgl-popup-content": {
      borderRadius: 0,
      padding: theme.spacing(1.5)
      // paddingBottom: 0,
      // left: "calc(50% - 20px)"
    }
  }
}));

function Popup({
  action = false,
  onClick = false,
  onClose = false,
  children,
  ...props
}) {
  const classes = useStyles({ action, ...props });
  return (
    <MapboxPopup
      className={classes.root}
      {...props}
      onClick={onClick}
      onClose={onClose}
      closeOnClick={true}
    >
      {children}
    </MapboxPopup>
  );
}

Popup.propTypes = {
  action: PropTypes.bool,
  onClick: PropTypes.func,
  children: PropTypes.node
};

export default Popup;
