import React, { useContext, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { NavigationControl } from "react-map-gl";
import InteractiveMap from "./InteractiveMap";
import Popup from "./Popup";
import { MapContext } from "./MapProvider";
import Typography from "@material-ui/core/Typography";
import Button from "../Buttons/Button";
import { makeStyles } from "@material-ui/styles";
import MessageList from "../Messages/MessageList";
import Message from "../Messages/Message";
import ButtonGroup from "../Buttons/ButtonGroup";
import { setFilter } from "../reducers/mapboxStyle";

const useStyles = makeStyles(theme => ({
  messages: {
    position: "absolute",
    zIndex: 999,
    left: theme.spacing(3),
    top: theme.spacing(3)
  },
  navigationControl: {
    position: "absolute",
    zIndex: 9,
    right: theme.spacing(3),
    bottom: theme.spacing(3)
  }
}));

function ParticipateMap() {
  /**
   * Handle map click and check for features.
   * If there's a feature we want to look at,
   * set it in state. Otherwise, set the state
   * to empty.
   */
  const { mapRef, flyTo, setViewState } = useContext(MapContext);
  const [feature, setFeature] = useState({});
  function handleClick(evt) {
    const { x, y } = evt;
    const features = mapRef.current.queryRenderedFeatures([x, y]);

    if (features[0] && features[0].properties.OBJECTID) {
      const { properties, geometry } = features[0];
      const [longitude, latitude] = geometry.coordinates;
      flyTo({
        latitude,
        longitude,
        zoom: 13,
        transitionDuration: 1000,
        pitch: 0
      });
      setFeature({
        properties,
        geometry: { latitude, longitude }
      });
    } else {
      setFeature({});
    }
  }

  const messages = [
    <Message
      key="1"
      text="Hey! We'd love you to participate in shaping your city. Select a category below to see what's close."
    />,
    <Message key="2">
      <ParticipateFilter />
    </Message>
  ];

  const { properties, geometry } = feature;
  const classes = useStyles();
  return (
    <>
      <div className={classes.messages}>
        <MessageList messages={messages} />
      </div>
      <InteractiveMap
        mapboxStyle="gisfeedback/cjwssqlwg0hwz1dpw7ls1tu4s"
        onClick={handleClick}
      >
        {geometry && (
          <Popup {...geometry} onClose={() => setFeature({})}>
            {properties && (
              <>
                <Typography variant="h5">{properties.Name}</Typography>
                <Typography variant="body2">
                  <strong>Category:</strong> {properties.Category}
                </Typography>
                <Typography variant="body2">
                  <strong>Place:</strong> {properties.PlaceCategory}
                </Typography>
                <Typography variant="body2" gutterBottom>
                  <strong>Status:</strong> {properties.Status}
                </Typography>
                <Button
                  onClick={() => window.open(properties.Website, "_blank")}
                >
                  More info
                </Button>
              </>
            )}
          </Popup>
        )}
        <div className={classes.navigationControl}>
          <NavigationControl onViewportChange={vp => setViewState(vp)} />
        </div>
      </InteractiveMap>
    </>
  );
}

export default ParticipateMap;

function ParticipateFilter() {
  const options = [
    {
      value: "Planning, Design and Renewal",
      label: "Planning, Design and Renewal"
    },
    { value: "Major Projects", label: "Major Projects" },
    { value: "Transport and Movement", label: "Transport and Movement" },
    { value: "Parks and Recreation", label: "Parks and Recreation" },
    { value: "Community", label: "Community" },
    { value: "Health and Wellbeing", label: "Health and Wellbeing" },
    { value: "Arts and Culture", label: "Arts and Culture" },
    { value: "all", label: "Show All" }
  ];

  const [selection, setSelection] = useState("all");
  const dispatch = useDispatch();
  useEffect(() => {
    if (selection !== undefined) {
      dispatch(
        setFilter([
          "ParticipateEngagements",
          selection === "all"
            ? ["has", "Category"]
            : ["==", ["get", "Category"], selection]
        ])
      );
    }
  }, [selection]);

  return (
    <ButtonGroup
      options={options}
      value={selection}
      onChange={s => setSelection(s.value)}
      compact={false}
    />
  );
}
