import React from "react";
import { useDispatch } from "react-redux";
import { addSource, addLayer } from "../reducers/mapboxStyle";
import InteractiveMap from "./InteractiveMap";

export default function MapboxMap() {
  const dispatch = useDispatch();
  /**
   * TEMPORARY
   */
  function addSourceAndLayers() {
    dispatch(
      addSource({
        id: "traffic",
        source: {
          url: "mapbox://mapbox.mapbox-traffic-v1",
          type: "vector"
        }
      })
    );
    dispatch(
      addLayer({
        layer: {
          id: "traffic",
          source: "traffic",
          "source-layer": "traffic",
          type: "line",
          paint: {
            "line-width": 4,
            "line-color": "#0ff"
          }
        }
      })
    );
  }

  return <InteractiveMap onStyleLoaded={addSourceAndLayers} />;
}
