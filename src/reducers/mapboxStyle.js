import { createSlice } from "redux-starter-kit";
import { Map, fromJS } from "immutable";
import Axios from "axios";

const { actions, reducer } = createSlice({
  initialState: new Map(),
  reducers: {
    setMapStyle: (mapStyle, { payload }) => fromJS(payload),

    addSource: (mapStyle, { payload: { id, source } }) => {
      /**
       * Check we're ready and have all the things we need
       */
      if (typeof mapStyle !== "object") {
        console.log("map style not initialised or on controller.");
        return;
      }
      if (!id || !source) {
        console.warn("no config or id for source");
        return;
      }

      if (typeof mapStyle !== "string" && !mapStyle.hasIn(["sources", [id]])) {
        const nextMapStyle = mapStyle.setIn(["sources", id], fromJS(source));
        return nextMapStyle;
      }
    },

    addLayer: (mapStyle, { payload: { layer, beforeId } }) => {
      if (typeof mapStyle !== "object") {
        console.log("map style not initialised or on controller.");
        return;
      }
      const layerIdx = getLayerIndex(mapStyle, layer.id);
      const insertIdx = beforeId
        ? getLayerIndex(mapStyle, beforeId) - 1
        : mapStyle.get("layers").size;
      let nextMapStyle;
      if (layerIdx === -1) {
        nextMapStyle = mapStyle.updateIn(["layers"], layers =>
          layers.splice(insertIdx, 0, fromJS(layer))
        );
      } else {
        nextMapStyle = mapStyle.updateIn(["layers", layerIdx], () =>
          fromJS(layer)
        );
      }
      return nextMapStyle;
    },

    setFilter: (mapStyle, { payload: [id, filter] }) => {
      if (typeof mapStyle !== "object") {
        console.log("map style not initialised or on controller.");
        return;
      }
      if (!id || !filter.length >= 2) {
        console.warn("no id supplied or incorrect mapbox filter");
        return;
      }

      const layerIdx = getLayerIndex(mapStyle, id);

      if (layerIdx !== -1) {
        const nextMapStyle = mapStyle.updateIn(
          ["layers", layerIdx, "filter"],
          () => filter
        );

        return nextMapStyle;
      }
    }
  },
  slice: "mapStyle"
});

export default reducer;

export const { setMapStyle, addSource, addLayer, setFilter } = actions;

// const getLayerIndex = (mapStyle, id) =>
//   mapStyle.loyers.findIndex(layer => layer.id === id);
const getLayerIndex = (mapStyle, id) =>
  mapStyle.get("layers").findIndex(layer => layer.get("id") === id);

/**
 * Load map style from mapbox api
 */
const mapboxAPIBase = "https://api.mapbox.com";
const key =
  "pk.eyJ1IjoiZ2lzZmVlZGJhY2siLCJhIjoiY2l2eDJndmtjMDFkeTJvcHM4YTNheXZtNyJ9.-HNJNch_WwLIAifPgzW2Ig";
export function loadMapStyle({ styleId }) {
  return async function loadStyleFromMapboxAPI(dispatch) {
    const { data } = await Axios.get(
      `${mapboxAPIBase}/styles/v1/${styleId}?access_token=${key}`
    );
    // dispatch(setMapStyle(fromJS({ ...data, apiLoaded: true })));
    dispatch(setMapStyle({ ...data, apiLoaded: true }));
  };
}
