import { combineReducers } from "redux-starter-kit";
import mapboxStyleReducer from "./mapboxStyle";
import mapViewstateReducer from "./mapViewstate";

export default combineReducers({
  style: mapboxStyleReducer,
  view: mapViewstateReducer
});
