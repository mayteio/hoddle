import React from "react";

import styled from "styled-components";

import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Typography from "@material-ui/core/Typography";

export default ({
  options,
  value,
  label,
  onChange,
  pop = "#e50e56",
  compact = true,
  disabled = false,
  ...rest
}) => (
  <FormControl fullWidth={true} {...rest}>
    {label && <FormLabel>{label}</FormLabel>}
    <Wrap>
      <Options disabled={disabled}>
        {options.map(option => {
          const Icon =
            option.icon &&
            styled(option.icon)({
              width: "48px !important",
              height: "48px !important",
              display: "block",
              color: pop
            });
          return (
            <Option
              key={option.value}
              active={value === option.value}
              onClick={() => onChange(option)}
              pop={pop}
              compact={compact}
            >
              {Icon && <Icon />}
              {option.title && (
                <Typography variant="h4" align="center">
                  {option.title}
                </Typography>
              )}
              <Label
                align="center"
                dangerouslySetInnerHTML={{ __html: option.label }}
              />
            </Option>
          );
        })}
      </Options>
    </Wrap>
  </FormControl>
);

const Wrap = styled.div`
  display: inline-block;
  opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
  pointer-events: ${({ disabled }) => (disabled ? "none" : "default")};

  /* justify-content: space-between; */
`;

const Options = styled.ul`
  padding-left: 0;
  margin-top: 0;
  margin-bottom: 12px;
  border-radius: 4px;
  display: inline-flex;
  flex-wrap: wrap;

  max-width: ${({ compact }) => !compact && "600px"}
    ${({ compact }) =>
      compact &&
      `box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),
    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);`};
`;

const Option = styled.li`
  list-style: none;

  svg {
    fill: #080809;
  }

  ${({ compact }) =>
    !compact &&
    `box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),
    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);`}
  display: flex;
  padding: 8px 12px;
  border-right: 1px solid #f2f3f4;
  flex-direction: column;
  cursor: pointer;
  background-color: #fff;
  transition: all 250ms linear;
  align-items: center;
  justify-content: center;
  text-transform: "uppercase";
  max-width: 140px;

  ${({ compact }) => {
    return compact
      ? `

  &:first-child {
    border-radius: 4px 0 0 4px;
  }
  &:last-child {
    border-right: none;
    border-radius: 0 4px 4px 0;
  }
  `
      : `
  border-radius: 4px; 

  margin-right: 12px;
  margin-bottom: 12px;
  `;
  }}

  p {
    color: ${({ pop }) => pop};
    font-weight: bold;
  }

  h4 {
    color: #080809;
  }

  &:hover {
    background-color: #d4d6db;

    svg {
      fill: ${({ pop }) => pop};
    }
  }

  ${({ active, pop }) =>
    active &&
    `
    background-color: ${pop};
    border-color: ${pop};

    svg {
      fill: #fff
    }

    p, h4 {
      color: #fff
    }

    &:hover {
      background-color: ${pop};
      border-color: ${pop};

      svg {
        fill: #fff
      }

      p, h4 {
        color: #fff
      }
    }
  `}
`;

const Label = styled(Typography)`
  text-transform: uppercase;
  font-weight: bold;
  font-size: 12px !important;
`;
