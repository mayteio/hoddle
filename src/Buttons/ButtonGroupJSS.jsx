import React from "react";

import { makeStyles } from "@material-ui/styles";

import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Typography from "@material-ui/core/Typography";

export default function ButtonGroup({
  options,
  value,
  label,
  onChange,
  color = "primary",
  compact = true,
  disabled = false,
  ...rest
}) {
  const classes = useStyles({ compact, disabled, color });
  console.log(classes);
  return (
    <FormControl fullWidth={true} {...rest}>
      {label && <FormLabel>{label}</FormLabel>}
      <div className={classes.root}>
        <ul className={classes.options} disabled={disabled}>
          {options.map(option => {
            const Icon = option.icon;
            return (
              <li
                className={
                  value === option.value ? classes.active : classes.option
                }
                key={option.value}
                onClick={() => onChange(option)}
              >
                {Icon && <Icon className={classes.icon} />}
                {option.title && (
                  <Typography variant="h4" align="center">
                    {option.title}
                  </Typography>
                )}
                <Typography
                  className={classes.label}
                  variant="body2"
                  align="center"
                  dangerouslySetInnerHTML={{ __html: option.label }}
                />
              </li>
            );
          })}
        </ul>
      </div>
    </FormControl>
  );
}

const staticOptionStyles = {
  display: "flex",
  flexDirection: "column",
  cursor: "pointer",
  transition: "all 250ms linear",
  alignItems: "center",
  justifyContent: "center",
  textTransform: "uppercase"
};

const useStyles = makeStyles(theme => ({
  root: {
    display: "inline-block",
    opacity: ({ disabled }) => (disabled ? 0.5 : 1),
    pointerEvents: ({ disabled }) => (disabled ? "none" : "default")
  },
  options: {
    paddingLeft: 0,
    marginTop: 0,
    marginBottom: theme.spacing(1.5),
    borderRadius: 4,
    display: "inline-flex",
    boxShadow: ({ compact }) =>
      compact
        ? `0px 1px 5px 0px rgba(0, 0, 0, 0.2),
    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)`
        : "none"
  },

  icon: {
    width: 48,
    height: 48,
    svg: { fill: "#fff" }
  },

  option: ({ compact, color }) => {
    const base = {
      ...staticOptionStyles,
      backgroundColor: theme.palette.grey100,
      padding: `${theme.spacing(2)}px ${theme.spacing(3)}px`,
      "& svg": {
        fill: theme.palette.grey800
      }
    };
    return compact
      ? {
          ...base,

          borderRight: `1px solid ${theme.palette.grey100}`,
          "&:first-child": {
            borderRadius: "4px 0 0 4px"
          },
          "&:last-child": {
            borderRight: "none",
            borderRadius: "0 4px 4px 0"
          }
        }
      : {
          ...base,
          boxShadow: `0px 1px 5px 0px rgba(0, 0, 0, 0.2),
            0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)`,

          borderRadius: 4,
          marginRight: 24,
          marginBottom: 24
        };
  },

  active: ({ color }) => ({
    extend: "option"
    // backgroundColor: theme.palette[color]
  }),

  label: {
    textTransform: "uppercase",
    fontWeight: "bold",
    color: ({ active }) =>
      active ? theme.palette.grey100 : theme.palette.grey800
  }
}));
