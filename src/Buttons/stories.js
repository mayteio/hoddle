import React from "react";
import { storiesOf } from "@storybook/react";

import Button from "./Button";
import ButtonGroupDemo from "./ButtonGroupDemo";

storiesOf("Buttons", module)
  .add("default", () => <Button>Button</Button>)
  .add("Button Group", () => <ButtonGroupDemo />);
