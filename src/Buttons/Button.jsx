import React from "react";
import PropTypes from "prop-types";
import MuiButton from "@material-ui/core/Button";

function Button({
  variant = "contained",
  color = "primary",
  children,
  ...props
}) {
  return (
    <MuiButton variant={variant} color={color} {...props}>
      {children}
    </MuiButton>
  );
}

Button.propTypes = {};

export default Button;
