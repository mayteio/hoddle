import React, { useState, useEffect } from "react";
import HomeIcon from "@material-ui/icons/Home";
import TramIcon from "@material-ui/icons/Tram";
import ButtonGroup from "./ButtonGroupJSS";
/**
 * Demo for the ButtonGroup story
 */
export default function ButtonGroupDemo() {
  const [selection, setSelection] = useState();
  const options = [
    { value: "1", label: "Option 1", icon: TramIcon },
    { value: "2", label: "Option 2", icon: HomeIcon }
  ];

  useEffect(() => {
    console.log("selection changed", selection);
  }, [selection]);

  return (
    <>
      <ButtonGroup
        options={options}
        value={selection}
        onChange={s => setSelection(s.value)}
        compact={false}
      />
      <ButtonGroup
        options={options}
        value={selection}
        onChange={s => setSelection(s.value)}
      />
    </>
  );
}
