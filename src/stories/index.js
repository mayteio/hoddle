import React from "react";

import { storiesOf } from "@storybook/react";

import MessageContent from "../Messages/MessageContent";

storiesOf("Messages", module).add("Message Content", () => (
  <MessageContent>Text here</MessageContent>
));
