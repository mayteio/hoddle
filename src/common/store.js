import { configureStore } from "redux-starter-kit";
import thunk from "redux-thunk";
import rootMapReducer from "../reducers";

const store = configureStore({
  reducer: {
    map: rootMapReducer
  },
  middleware: [thunk]
});

export default store;
