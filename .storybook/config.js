import { configure, addDecorator } from "@storybook/react";
import { muiTheme } from "storybook-addon-material-ui/dist/muiTheme";
import dark from "../src/common/dark.theme";

const req = require.context("../src/", true, /stories\.js$/);

function loadStories() {
  req.keys().forEach(req);
}

addDecorator(muiTheme([dark]));
console.log(dark);

configure(loadStories, module);
