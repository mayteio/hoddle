"use strict";

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _MessageContent = _interopRequireDefault(require("../Messages/MessageContent"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _react2.storiesOf)("Messages", module).add("Message Content", function () {
  return _react.default.createElement(_MessageContent.default, null, "Text here");
});