"use strict";

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _a11yReactEmoji = _interopRequireDefault(require("a11y-react-emoji"));

var _Message = _interopRequireDefault(require("./Message"));

var _MessageText = _interopRequireDefault(require("./MessageText"));

var _MessageBubble = _interopRequireDefault(require("./MessageBubble"));

var _MessageList = _interopRequireDefault(require("./MessageList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var text = _react.default.createElement(_react.default.Fragment, null, "Hello ", _react.default.createElement(_a11yReactEmoji.default, {
  symbol: "\uD83D\uDC4B"
}));

var MessageTextComponent = function MessageTextComponent() {
  return _react.default.createElement(_MessageText.default, null, text);
};

var MessageHelperComponent = function MessageHelperComponent() {
  return _react.default.createElement(_Message.default, {
    text: text
  });
};

(0, _react2.storiesOf)("Messages", module).add("Message Text", function () {
  return _react.default.createElement(MessageTextComponent, null);
}).add("Message Bubble", function () {
  return _react.default.createElement(_MessageBubble.default, null, _react.default.createElement(MessageTextComponent, null));
}).add("Message Helper", function () {
  return _react.default.createElement(MessageHelperComponent, null);
}).add("Message List", function () {
  return _react.default.createElement(_MessageList.default, {
    messages: [_react.default.createElement(MessageHelperComponent, {
      key: 1
    }), _react.default.createElement(MessageHelperComponent, {
      key: 2
    })]
  });
});