"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactSpring = require("react-spring");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function MessageList(_ref) {
  var messages = _ref.messages;
  var transitions = (0, _reactSpring.useTransition)(messages, function (item) {
    return item.key;
  }, states);
  return _react.default.createElement(_react.default.Fragment, null, transitions.map(function (_ref2) {
    var item = _ref2.item,
        props = _ref2.props,
        key = _ref2.key;
    return _react.default.createElement(_reactSpring.animated.div, {
      key: key,
      style: props
    }, item);
  }));
}

var _default = MessageList;
exports.default = _default;
var states = {
  from: {
    transform: "translate3d(-100px, 20px, -20px)",
    opacity: 0
  },
  enter: {
    transform: "translate3d(0%, 0, 0)",
    opacity: 1
  },
  leave: {
    transform: "translate3d(-100%, 0, 0)",
    opacity: 0
  },
  config: {
    friction: 20,
    mass: 1,
    tension: 500
  },
  trail: 50
};