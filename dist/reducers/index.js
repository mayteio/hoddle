"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reduxStarterKit = require("redux-starter-kit");

var _mapboxStyle = _interopRequireDefault(require("./mapboxStyle"));

var _mapViewstate = _interopRequireDefault(require("./mapViewstate"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = (0, _reduxStarterKit.combineReducers)({
  style: _mapboxStyle.default,
  view: _mapViewstate.default
});

exports.default = _default;