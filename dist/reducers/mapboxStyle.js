"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadMapStyle = loadMapStyle;
exports.setFilter = exports.addLayer = exports.addSource = exports.setMapStyle = exports.default = void 0;

var _reduxStarterKit = require("redux-starter-kit");

var _immutable = require("immutable");

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _createSlice = (0, _reduxStarterKit.createSlice)({
  initialState: new _immutable.Map(),
  reducers: {
    setMapStyle: function setMapStyle(mapStyle, _ref) {
      var payload = _ref.payload;
      return (0, _immutable.fromJS)(payload);
    },
    addSource: function addSource(mapStyle, _ref2) {
      var _ref2$payload = _ref2.payload,
          id = _ref2$payload.id,
          source = _ref2$payload.source;

      /**
       * Check we're ready and have all the things we need
       */
      if (_typeof(mapStyle) !== "object") {
        console.log("map style not initialised or on controller.");
        return;
      }

      if (!id || !source) {
        console.warn("no config or id for source");
        return;
      }

      if (typeof mapStyle !== "string" && !mapStyle.hasIn(["sources", [id]])) {
        var nextMapStyle = mapStyle.setIn(["sources", id], (0, _immutable.fromJS)(source));
        return nextMapStyle;
      }
    },
    addLayer: function addLayer(mapStyle, _ref3) {
      var _ref3$payload = _ref3.payload,
          layer = _ref3$payload.layer,
          beforeId = _ref3$payload.beforeId;

      if (_typeof(mapStyle) !== "object") {
        console.log("map style not initialised or on controller.");
        return;
      }

      var layerIdx = getLayerIndex(mapStyle, layer.id);
      var insertIdx = beforeId ? getLayerIndex(mapStyle, beforeId) - 1 : mapStyle.get("layers").size;
      var nextMapStyle;

      if (layerIdx === -1) {
        nextMapStyle = mapStyle.updateIn(["layers"], function (layers) {
          return layers.splice(insertIdx, 0, (0, _immutable.fromJS)(layer));
        });
      } else {
        nextMapStyle = mapStyle.updateIn(["layers", layerIdx], function () {
          return (0, _immutable.fromJS)(layer);
        });
      }

      return nextMapStyle;
    },
    setFilter: function setFilter(mapStyle, _ref4) {
      var _ref4$payload = _slicedToArray(_ref4.payload, 2),
          id = _ref4$payload[0],
          filter = _ref4$payload[1];

      if (_typeof(mapStyle) !== "object") {
        console.log("map style not initialised or on controller.");
        return;
      }

      if (!id || !filter.length >= 2) {
        console.warn("no id supplied or incorrect mapbox filter");
        return;
      }

      var layerIdx = getLayerIndex(mapStyle, id);

      if (layerIdx !== -1) {
        var nextMapStyle = mapStyle.updateIn(["layers", layerIdx, "filter"], function () {
          return filter;
        });
        return nextMapStyle;
      }
    }
  },
  slice: "mapStyle"
}),
    actions = _createSlice.actions,
    reducer = _createSlice.reducer;

var _default = reducer;
exports.default = _default;
var setMapStyle = actions.setMapStyle,
    addSource = actions.addSource,
    addLayer = actions.addLayer,
    setFilter = actions.setFilter; // const getLayerIndex = (mapStyle, id) =>
//   mapStyle.loyers.findIndex(layer => layer.id === id);

exports.setFilter = setFilter;
exports.addLayer = addLayer;
exports.addSource = addSource;
exports.setMapStyle = setMapStyle;

var getLayerIndex = function getLayerIndex(mapStyle, id) {
  return mapStyle.get("layers").findIndex(function (layer) {
    return layer.get("id") === id;
  });
};
/**
 * Load map style from mapbox api
 */


var mapboxAPIBase = "https://api.mapbox.com";
var key = "pk.eyJ1IjoiZ2lzZmVlZGJhY2siLCJhIjoiY2l2eDJndmtjMDFkeTJvcHM4YTNheXZtNyJ9.-HNJNch_WwLIAifPgzW2Ig";

function loadMapStyle(_ref5) {
  var styleId = _ref5.styleId;
  return (
    /*#__PURE__*/
    function () {
      var _loadStyleFromMapboxAPI = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(dispatch) {
        var _ref6, data;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _axios.default.get("".concat(mapboxAPIBase, "/styles/v1/").concat(styleId, "?access_token=").concat(key));

              case 2:
                _ref6 = _context.sent;
                data = _ref6.data;
                // dispatch(setMapStyle(fromJS({ ...data, apiLoaded: true })));
                dispatch(setMapStyle(_objectSpread({}, data, {
                  apiLoaded: true
                })));

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function loadStyleFromMapboxAPI(_x) {
        return _loadStyleFromMapboxAPI.apply(this, arguments);
      }

      return loadStyleFromMapboxAPI;
    }()
  );
}