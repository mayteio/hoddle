"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ButtonGroup;

var _react = _interopRequireDefault(require("react"));

var _styles = require("@material-ui/styles");

var _FormControl = _interopRequireDefault(require("@material-ui/core/FormControl"));

var _FormLabel = _interopRequireDefault(require("@material-ui/core/FormLabel"));

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ButtonGroup(_ref) {
  var options = _ref.options,
      value = _ref.value,
      label = _ref.label,
      onChange = _ref.onChange,
      _ref$color = _ref.color,
      color = _ref$color === void 0 ? "primary" : _ref$color,
      _ref$compact = _ref.compact,
      compact = _ref$compact === void 0 ? true : _ref$compact,
      _ref$disabled = _ref.disabled,
      disabled = _ref$disabled === void 0 ? false : _ref$disabled,
      rest = _objectWithoutProperties(_ref, ["options", "value", "label", "onChange", "color", "compact", "disabled"]);

  var classes = useStyles({
    compact: compact,
    disabled: disabled,
    color: color
  });
  console.log(classes);
  return _react.default.createElement(_FormControl.default, _extends({
    fullWidth: true
  }, rest), label && _react.default.createElement(_FormLabel.default, null, label), _react.default.createElement("div", {
    className: classes.root
  }, _react.default.createElement("ul", {
    className: classes.options,
    disabled: disabled
  }, options.map(function (option) {
    var Icon = option.icon;
    return _react.default.createElement("li", {
      className: value === option.value ? classes.active : classes.option,
      key: option.value,
      onClick: function onClick() {
        return onChange(option);
      }
    }, Icon && _react.default.createElement(Icon, {
      className: classes.icon
    }), option.title && _react.default.createElement(_Typography.default, {
      variant: "h4",
      align: "center"
    }, option.title), _react.default.createElement(_Typography.default, {
      className: classes.label,
      variant: "body2",
      align: "center",
      dangerouslySetInnerHTML: {
        __html: option.label
      }
    }));
  }))));
}

var staticOptionStyles = {
  display: "flex",
  flexDirection: "column",
  cursor: "pointer",
  transition: "all 250ms linear",
  alignItems: "center",
  justifyContent: "center",
  textTransform: "uppercase"
};
var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      display: "inline-block",
      opacity: function opacity(_ref2) {
        var disabled = _ref2.disabled;
        return disabled ? 0.5 : 1;
      },
      pointerEvents: function pointerEvents(_ref3) {
        var disabled = _ref3.disabled;
        return disabled ? "none" : "default";
      }
    },
    options: {
      paddingLeft: 0,
      marginTop: 0,
      marginBottom: theme.spacing(1.5),
      borderRadius: 4,
      display: "inline-flex",
      boxShadow: function boxShadow(_ref4) {
        var compact = _ref4.compact;
        return compact ? "0px 1px 5px 0px rgba(0, 0, 0, 0.2),\n    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)" : "none";
      }
    },
    icon: {
      width: 48,
      height: 48,
      svg: {
        fill: "#fff"
      }
    },
    option: function option(_ref5) {
      var compact = _ref5.compact,
          color = _ref5.color;

      var base = _objectSpread({}, staticOptionStyles, {
        backgroundColor: theme.palette.grey100,
        padding: "".concat(theme.spacing(2), "px ").concat(theme.spacing(3), "px"),
        "& svg": {
          fill: theme.palette.grey800
        }
      });

      return compact ? _objectSpread({}, base, {
        borderRight: "1px solid ".concat(theme.palette.grey100),
        "&:first-child": {
          borderRadius: "4px 0 0 4px"
        },
        "&:last-child": {
          borderRight: "none",
          borderRadius: "0 4px 4px 0"
        }
      }) : _objectSpread({}, base, {
        boxShadow: "0px 1px 5px 0px rgba(0, 0, 0, 0.2),\n            0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)",
        borderRadius: 4,
        marginRight: 24,
        marginBottom: 24
      });
    },
    active: function active(_ref6) {
      var color = _ref6.color;
      return {
        extend: "option" // backgroundColor: theme.palette[color]

      };
    },
    label: {
      textTransform: "uppercase",
      fontWeight: "bold",
      color: function color(_ref7) {
        var active = _ref7.active;
        return active ? theme.palette.grey100 : theme.palette.grey800;
      }
    }
  };
});