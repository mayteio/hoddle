"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _FormControl = _interopRequireDefault(require("@material-ui/core/FormControl"));

var _FormLabel = _interopRequireDefault(require("@material-ui/core/FormLabel"));

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  text-transform: uppercase;\n  font-weight: bold;\n  font-size: 12px !important;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  list-style: none;\n\n  svg {\n    fill: #080809;\n  }\n\n  ", "\n  display: flex;\n  padding: 8px 12px;\n  border-right: 1px solid #f2f3f4;\n  flex-direction: column;\n  cursor: pointer;\n  background-color: #fff;\n  transition: all 250ms linear;\n  align-items: center;\n  justify-content: center;\n  text-transform: \"uppercase\";\n  max-width: 140px;\n\n  ", "\n\n  p {\n    color: ", ";\n    font-weight: bold;\n  }\n\n  h4 {\n    color: #080809;\n  }\n\n  &:hover {\n    background-color: #d4d6db;\n\n    svg {\n      fill: ", ";\n    }\n  }\n\n  ", "\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  padding-left: 0;\n  margin-top: 0;\n  margin-bottom: 12px;\n  border-radius: 4px;\n  display: inline-flex;\n  flex-wrap: wrap;\n\n  max-width: ", "\n    ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: inline-block;\n  opacity: ", ";\n  pointer-events: ", ";\n\n  /* justify-content: space-between; */\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var _default = function _default(_ref) {
  var options = _ref.options,
      value = _ref.value,
      label = _ref.label,
      onChange = _ref.onChange,
      _ref$pop = _ref.pop,
      pop = _ref$pop === void 0 ? "#e50e56" : _ref$pop,
      _ref$compact = _ref.compact,
      compact = _ref$compact === void 0 ? true : _ref$compact,
      _ref$disabled = _ref.disabled,
      disabled = _ref$disabled === void 0 ? false : _ref$disabled,
      rest = _objectWithoutProperties(_ref, ["options", "value", "label", "onChange", "pop", "compact", "disabled"]);

  return _react.default.createElement(_FormControl.default, _extends({
    fullWidth: true
  }, rest), label && _react.default.createElement(_FormLabel.default, null, label), _react.default.createElement(Wrap, null, _react.default.createElement(Options, {
    disabled: disabled
  }, options.map(function (option) {
    var Icon = option.icon && (0, _styledComponents.default)(option.icon)({
      width: "48px !important",
      height: "48px !important",
      display: "block",
      color: pop
    });
    return _react.default.createElement(Option, {
      key: option.value,
      active: value === option.value,
      onClick: function onClick() {
        return onChange(option);
      },
      pop: pop,
      compact: compact
    }, Icon && _react.default.createElement(Icon, null), option.title && _react.default.createElement(_Typography.default, {
      variant: "h4",
      align: "center"
    }, option.title), _react.default.createElement(Label, {
      align: "center",
      dangerouslySetInnerHTML: {
        __html: option.label
      }
    }));
  }))));
};

exports.default = _default;

var Wrap = _styledComponents.default.div(_templateObject(), function (_ref2) {
  var disabled = _ref2.disabled;
  return disabled ? 0.5 : 1;
}, function (_ref3) {
  var disabled = _ref3.disabled;
  return disabled ? "none" : "default";
});

var Options = _styledComponents.default.ul(_templateObject2(), function (_ref4) {
  var compact = _ref4.compact;
  return !compact && "600px";
}, function (_ref5) {
  var compact = _ref5.compact;
  return compact && "box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),\n    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);";
});

var Option = _styledComponents.default.li(_templateObject3(), function (_ref6) {
  var compact = _ref6.compact;
  return !compact && "box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),\n    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);";
}, function (_ref7) {
  var compact = _ref7.compact;
  return compact ? "\n\n  &:first-child {\n    border-radius: 4px 0 0 4px;\n  }\n  &:last-child {\n    border-right: none;\n    border-radius: 0 4px 4px 0;\n  }\n  " : "\n  border-radius: 4px; \n\n  margin-right: 12px;\n  margin-bottom: 12px;\n  ";
}, function (_ref8) {
  var pop = _ref8.pop;
  return pop;
}, function (_ref9) {
  var pop = _ref9.pop;
  return pop;
}, function (_ref10) {
  var active = _ref10.active,
      pop = _ref10.pop;
  return active && "\n    background-color: ".concat(pop, ";\n    border-color: ").concat(pop, ";\n\n    svg {\n      fill: #fff\n    }\n\n    p, h4 {\n      color: #fff\n    }\n\n    &:hover {\n      background-color: ").concat(pop, ";\n      border-color: ").concat(pop, ";\n\n      svg {\n        fill: #fff\n      }\n\n      p, h4 {\n        color: #fff\n      }\n    }\n  ");
});

var Label = (0, _styledComponents.default)(_Typography.default)(_templateObject4());