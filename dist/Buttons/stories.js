"use strict";

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _Button = _interopRequireDefault(require("./Button"));

var _ButtonGroupDemo = _interopRequireDefault(require("./ButtonGroupDemo"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _react2.storiesOf)("Buttons", module).add("default", function () {
  return _react.default.createElement(_Button.default, null, "Button");
}).add("Button Group", function () {
  return _react.default.createElement(_ButtonGroupDemo.default, null);
});