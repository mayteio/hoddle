"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = MapboxMap;

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _mapboxStyle = require("../reducers/mapboxStyle");

var _InteractiveMap = _interopRequireDefault(require("./InteractiveMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function MapboxMap() {
  var dispatch = (0, _reactRedux.useDispatch)();
  /**
   * TEMPORARY
   */

  function addSourceAndLayers() {
    dispatch((0, _mapboxStyle.addSource)({
      id: "traffic",
      source: {
        url: "mapbox://mapbox.mapbox-traffic-v1",
        type: "vector"
      }
    }));
    dispatch((0, _mapboxStyle.addLayer)({
      layer: {
        id: "traffic",
        source: "traffic",
        "source-layer": "traffic",
        type: "line",
        paint: {
          "line-width": 4,
          "line-color": "#0ff"
        }
      }
    }));
  }

  return _react.default.createElement(_InteractiveMap.default, {
    onStyleLoaded: addSourceAndLayers
  });
}