"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

var _Popup = _interopRequireDefault(require("./Popup"));

var _InteractiveMap = _interopRequireDefault(require("./InteractiveMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function PopupMap() {
  var layout = {
    longitude: 144.96,
    latitude: -37.81
  };
  return _react.default.createElement(_InteractiveMap.default, null, _react.default.createElement(_Popup.default, layout, _react.default.createElement(_Typography.default, {
    variant: "h4"
  }, "JSX here"), _react.default.createElement(_Typography.default, {
    variant: "body2"
  }, "TODO: add action buttons & markers, style more like com")));
}

var _default = PopupMap;
exports.default = _default;