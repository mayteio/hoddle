"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _reactMapGl = require("react-map-gl");

var _InteractiveMap = _interopRequireDefault(require("./InteractiveMap"));

var _Popup = _interopRequireDefault(require("./Popup"));

var _MapProvider = require("./MapProvider");

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

var _Button = _interopRequireDefault(require("../Buttons/Button"));

var _styles = require("@material-ui/styles");

var _MessageList = _interopRequireDefault(require("../Messages/MessageList"));

var _Message = _interopRequireDefault(require("../Messages/Message"));

var _ButtonGroup = _interopRequireDefault(require("../Buttons/ButtonGroup"));

var _mapboxStyle = require("../reducers/mapboxStyle");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    messages: {
      position: "absolute",
      zIndex: 999,
      left: theme.spacing(3),
      top: theme.spacing(3)
    },
    navigationControl: {
      position: "absolute",
      zIndex: 9,
      right: theme.spacing(3),
      bottom: theme.spacing(3)
    }
  };
});

function ParticipateMap() {
  /**
   * Handle map click and check for features.
   * If there's a feature we want to look at,
   * set it in state. Otherwise, set the state
   * to empty.
   */
  var _useContext = (0, _react.useContext)(_MapProvider.MapContext),
      mapRef = _useContext.mapRef,
      flyTo = _useContext.flyTo,
      setViewState = _useContext.setViewState;

  var _useState = (0, _react.useState)({}),
      _useState2 = _slicedToArray(_useState, 2),
      feature = _useState2[0],
      setFeature = _useState2[1];

  function handleClick(evt) {
    var x = evt.x,
        y = evt.y;
    var features = mapRef.current.queryRenderedFeatures([x, y]);

    if (features[0] && features[0].properties.OBJECTID) {
      var _features$ = features[0],
          _properties = _features$.properties,
          _geometry = _features$.geometry;

      var _geometry$coordinates = _slicedToArray(_geometry.coordinates, 2),
          longitude = _geometry$coordinates[0],
          latitude = _geometry$coordinates[1];

      flyTo({
        latitude: latitude,
        longitude: longitude,
        zoom: 13,
        transitionDuration: 1000,
        pitch: 0
      });
      setFeature({
        properties: _properties,
        geometry: {
          latitude: latitude,
          longitude: longitude
        }
      });
    } else {
      setFeature({});
    }
  }

  var messages = [_react.default.createElement(_Message.default, {
    key: "1",
    text: "Hey! We'd love you to participate in shaping your city. Select a category below to see what's close."
  }), _react.default.createElement(_Message.default, {
    key: "2"
  }, _react.default.createElement(ParticipateFilter, null))];
  var properties = feature.properties,
      geometry = feature.geometry;
  var classes = useStyles();
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("div", {
    className: classes.messages
  }, _react.default.createElement(_MessageList.default, {
    messages: messages
  })), _react.default.createElement(_InteractiveMap.default, {
    mapboxStyle: "gisfeedback/cjwssqlwg0hwz1dpw7ls1tu4s",
    onClick: handleClick
  }, geometry && _react.default.createElement(_Popup.default, _extends({}, geometry, {
    onClose: function onClose() {
      return setFeature({});
    }
  }), properties && _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Typography.default, {
    variant: "h5"
  }, properties.Name), _react.default.createElement(_Typography.default, {
    variant: "body2"
  }, _react.default.createElement("strong", null, "Category:"), " ", properties.Category), _react.default.createElement(_Typography.default, {
    variant: "body2"
  }, _react.default.createElement("strong", null, "Place:"), " ", properties.PlaceCategory), _react.default.createElement(_Typography.default, {
    variant: "body2",
    gutterBottom: true
  }, _react.default.createElement("strong", null, "Status:"), " ", properties.Status), _react.default.createElement(_Button.default, {
    onClick: function onClick() {
      return window.open(properties.Website, "_blank");
    }
  }, "More info"))), _react.default.createElement("div", {
    className: classes.navigationControl
  }, _react.default.createElement(_reactMapGl.NavigationControl, {
    onViewportChange: function onViewportChange(vp) {
      return setViewState(vp);
    }
  }))));
}

var _default = ParticipateMap;
exports.default = _default;

function ParticipateFilter() {
  var options = [{
    value: "Planning, Design and Renewal",
    label: "Planning, Design and Renewal"
  }, {
    value: "Major Projects",
    label: "Major Projects"
  }, {
    value: "Transport and Movement",
    label: "Transport and Movement"
  }, {
    value: "Parks and Recreation",
    label: "Parks and Recreation"
  }, {
    value: "Community",
    label: "Community"
  }, {
    value: "Health and Wellbeing",
    label: "Health and Wellbeing"
  }, {
    value: "Arts and Culture",
    label: "Arts and Culture"
  }, {
    value: "all",
    label: "Show All"
  }];

  var _useState3 = (0, _react.useState)("all"),
      _useState4 = _slicedToArray(_useState3, 2),
      selection = _useState4[0],
      setSelection = _useState4[1];

  var dispatch = (0, _reactRedux.useDispatch)();
  (0, _react.useEffect)(function () {
    if (selection !== undefined) {
      dispatch((0, _mapboxStyle.setFilter)(["ParticipateEngagements", selection === "all" ? ["has", "Category"] : ["==", ["get", "Category"], selection]]));
    }
  }, [selection]);
  return _react.default.createElement(_ButtonGroup.default, {
    options: options,
    value: selection,
    onChange: function onChange(s) {
      return setSelection(s.value);
    },
    compact: false
  });
}