"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactRedux = require("react-redux");

var _reactMapGl = require("react-map-gl");

require("mapbox-gl/dist/mapbox-gl.css");

var _react2 = _interopRequireDefault(require("@deck.gl/react"));

var _MapProvider = require("./MapProvider");

var _mapboxStyle = require("../reducers/mapboxStyle");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function InteractiveMap(_ref) {
  var _ref$mapboxStyle = _ref.mapboxStyle,
      mapboxStyle = _ref$mapboxStyle === void 0 ? "mapbox/dark-v9" : _ref$mapboxStyle,
      _ref$mapboxApiAccessT = _ref.mapboxApiAccessToken,
      mapboxApiAccessToken = _ref$mapboxApiAccessT === void 0 ? "no-token" : _ref$mapboxApiAccessT,
      _ref$onStyleLoaded = _ref.onStyleLoaded,
      onStyleLoaded = _ref$onStyleLoaded === void 0 ? function () {} : _ref$onStyleLoaded,
      children = _ref.children,
      props = _objectWithoutProperties(_ref, ["mapboxStyle", "mapboxApiAccessToken", "onStyleLoaded", "children"]);

  /**
   * When the map is ready, get the current style from
   * the API ready to be modified in the store
   */
  var dispatch = (0, _reactRedux.useDispatch)();

  function onLoad() {
    dispatch((0, _mapboxStyle.loadMapStyle)({
      styleId: mapboxStyle
    }));
  }
  /**
   * Wait for style to load before adding any sources or layers
   */


  var style = (0, _reactRedux.useSelector)(function (state) {
    return state.map.style;
  });
  (0, _react.useEffect)(function () {
    if (style.get("apiLoaded") && style.size !== 0) {
      onStyleLoaded();
    }
  }, [style.get("apiLoaded"), onStyleLoaded]);
  /**
   * Get style and tokens to display the map
   */

  var mapStyle = (0, _reactRedux.useSelector)(function (state) {
    return state.map.style;
  });
  var token = process.env.REACT_MAPBOX_ACCESS_TOKEN || process.env.STORYBOOK_MAPBOX_ACCESS_TOKEN || mapboxApiAccessToken;

  var _useContext = (0, _react.useContext)(_MapProvider.MapContext),
      viewState = _useContext.viewState,
      setViewState = _useContext.setViewState,
      mapRef = _useContext.mapRef,
      deckRef = _useContext.deckRef;

  var _useState = (0, _react.useState)(undefined),
      _useState2 = _slicedToArray(_useState, 2),
      gl = _useState2[0],
      setGl = _useState2[1];

  return _react.default.createElement(_react2.default, _extends({
    controller: {
      touchRotate: true
    },
    viewState: viewState,
    onViewStateChange: function onViewStateChange(_ref2) {
      var viewState = _ref2.viewState;
      return setViewState(viewState);
    } // onWebGLInitialized={gl => setGl(gl)}
    ,
    ref: function ref(_ref4) {
      return deckRef.current = _ref4 && _ref4.deck;
    }
  }, props), _react.default.createElement(_reactMapGl.StaticMap, {
    onLoad: onLoad,
    mapStyle: mapStyle && mapStyle.get("apiLoaded") ? mapStyle : "mapbox://styles/".concat(mapboxStyle),
    mapboxApiAccessToken: token,
    ref: function ref(_ref3) {
      return mapRef.current = _ref3 && _ref3.getMap();
    } // gl={gl}

  }, children));
}

InteractiveMap.propTypes = {
  mapboxStyle: _propTypes.default.string,
  mapboxApiAccessToken: _propTypes.default.string,
  onStyleLoaded: _propTypes.default.func,
  children: _propTypes.default.node
};
var _default = InteractiveMap;
exports.default = _default;