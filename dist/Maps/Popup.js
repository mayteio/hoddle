"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactMapGl = require("react-map-gl");

var _styles = require("@material-ui/styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      minWidth: theme.spacing(10),
      zIndex: 8,
      "& .mapboxgl-popup": {},
      "& .mapboxgl-popup-content": {
        borderRadius: 0,
        padding: theme.spacing(1.5) // paddingBottom: 0,
        // left: "calc(50% - 20px)"

      }
    }
  };
});

function Popup(_ref) {
  var _ref$action = _ref.action,
      action = _ref$action === void 0 ? false : _ref$action,
      _ref$onClick = _ref.onClick,
      onClick = _ref$onClick === void 0 ? false : _ref$onClick,
      _ref$onClose = _ref.onClose,
      onClose = _ref$onClose === void 0 ? false : _ref$onClose,
      children = _ref.children,
      props = _objectWithoutProperties(_ref, ["action", "onClick", "onClose", "children"]);

  var classes = useStyles(_objectSpread({
    action: action
  }, props));
  return _react.default.createElement(_reactMapGl.Popup, _extends({
    className: classes.root
  }, props, {
    onClick: onClick,
    onClose: onClose,
    closeOnClick: true
  }), children);
}

Popup.propTypes = {
  action: _propTypes.default.bool,
  onClick: _propTypes.default.func,
  children: _propTypes.default.node
};
var _default = Popup;
exports.default = _default;