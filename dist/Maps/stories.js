"use strict";

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _reactRedux = require("react-redux");

var _store = _interopRequireDefault(require("../common/store"));

var _InteractiveMap = _interopRequireDefault(require("./InteractiveMap"));

var _MapProvider = require("./MapProvider");

var _ParticipateMap = _interopRequireDefault(require("./ParticipateMap"));

var _MapboxMap = _interopRequireDefault(require("./MapboxMap"));

var _PopupMap = _interopRequireDefault(require("./PopupMap"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var withReduxProvider = function withReduxProvider(story) {
  return _react.default.createElement(_reactRedux.Provider, {
    store: _store.default
  }, story());
};

var withMapProvider = function withMapProvider(story) {
  return _react.default.createElement(_MapProvider.MapProvider, null, story());
};

(0, _react2.storiesOf)("Maps", module).addDecorator(withReduxProvider).addDecorator(withMapProvider).add("Default Map", function () {
  return _react.default.createElement(_InteractiveMap.default, null);
}).add("Mapbox Layers Map", function () {
  return _react.default.createElement(_MapboxMap.default, null);
}).add("Map with popup", function () {
  return _react.default.createElement(_PopupMap.default, null);
}).add("Participate Map", function () {
  return _react.default.createElement(_ParticipateMap.default, null);
});