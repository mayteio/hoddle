"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reduxStarterKit = require("redux-starter-kit");

var _reduxThunk = _interopRequireDefault(require("redux-thunk"));

var _reducers = _interopRequireDefault(require("../reducers"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var store = (0, _reduxStarterKit.configureStore)({
  reducer: {
    map: _reducers.default
  },
  middleware: [_reduxThunk.default]
});
var _default = store;
exports.default = _default;