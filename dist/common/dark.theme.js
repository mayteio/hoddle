"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createMuiTheme = _interopRequireDefault(require("@material-ui/core/styles/createMuiTheme"));

require("../assets/fonts/fonts.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fontFamily = ["Gotham", "Roboto", "-apple-system", "BlinkMacSystemFont", '"Segoe UI"', '"Helvetica Neue"', "Arial", "sans-serif", '"Apple Color Emoji"', '"Segoe UI Emoji"', '"Segoe UI Symbol"'].join(",");
var headingFontFamily = "\"CoM\", ".concat(fontFamily);
/**
 * Primary colour palette is dark background.
 * Secondary is white background.
 */

var theme = (0, _createMuiTheme.default)({
  palette: {
    primary: {
      main: "#e50e56"
    },
    secondary: {
      main: "#277bb4"
    },
    black: "#000000",
    grey800: "#080809",
    grey600: "#23242b",
    grey400: "#3c404b",
    grey200: "#d4d6db",
    grey100: "#f2f3f4"
  },
  typography: {
    useNextVariants: true,
    // Use the system font instead of the default Roboto font.
    fontFamily: fontFamily,
    h1: {
      fontFamily: headingFontFamily
    },
    h2: {
      fontFamily: headingFontFamily
    },
    h3: {
      fontFamily: headingFontFamily
    },
    h4: {
      fontFamily: headingFontFamily
    },
    h5: {
      fontFamily: headingFontFamily
    },
    h6: {
      fontFamily: headingFontFamily,
      fontSize: 30
    }
  },
  overrides: {
    MuiListItemText: {
      primary: {// fontWeight: "bold"
      }
    },
    MuiTab: {
      label: {// color: "#fff"
      }
    }
  }
});
var _default = theme;
exports.default = _default;